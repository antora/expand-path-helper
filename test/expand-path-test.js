'use strict'

const assert = require('node:assert/strict')
const { describe, it } = require('node:test')
const os = require('node:os')
const ospath = require('node:path')
const expandPath = require('@antora/expand-path-helper')

describe('expandPath()', () => {
  describe('argument validation', () => {
    it('should throw error if path is undefined', () => {
      const expected = new TypeError('The "path" argument must be of type string. Received type undefined')
      const actual = expandPath
      assert.throws(actual, expected)
    })

    it('should throw error if path is not a string', () => {
      const inputPath = true
      const expected = new TypeError('The "path" argument must be of type string. Received type boolean')
      const actual = () => expandPath(inputPath)
      assert.throws(actual, expected)
    })
  })

  describe('absolute', () => {
    it('should not prepend base value to absolute path', () => {
      const inputPath = ospath.join(process.cwd(), 'dir')
      const expected = inputPath
      const actual = expandPath(inputPath, { base: os.tmpdir() })
      assert.equal(actual, expected)
    })

    it('should normalize absolute path', () => {
      let root = ospath.parse(process.cwd()).root
      if (ospath.sep === '\\') root = root.replaceAll(ospath.sep, '/')
      const inputPath = `${root}/dir/./path/to/..`
      const expected = ospath.join(root, 'dir', 'path')
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })
  })

  describe('relative', () => {
    it('should resolve relative path starting from cwd if base parameter is not given', () => {
      const inputPath = 'path/to/file'
      const expected = ospath.join(process.cwd(), inputPath)
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })

    it('should resolve relative path starting from value of cwd parameter if base parameter is not given', () => {
      const inputPath = 'path/to/file'
      const expected = ospath.join(os.homedir(), inputPath)
      const actual = expandPath(inputPath, { cwd: os.homedir() })
      assert.equal(actual, expected)
    })

    it('should resolve relative path starting from base value', () => {
      const inputPath = 'path/to/file'
      const base = os.tmpdir()
      const expected = ospath.join(base, inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should resolve relative path with single segment starting from base value', () => {
      const inputPath = 'folder'
      const base = os.tmpdir()
      const expected = ospath.join(base, inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should resolve relative path starting from pwd if base value is empty', () => {
      const inputPath = 'path/to/file'
      const base = ''
      const expected = ospath.join(process.cwd(), inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should resolve relative path starting from dot value if base value is .', () => {
      const inputPath = 'path/to/file'
      const base = '.'
      const dot = os.tmpdir()
      const expected = ospath.join(dot, inputPath)
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should resolve relative path starting from pwd if both base and dot values are .', () => {
      const inputPath = 'path/to/file'
      const base = '.'
      const dot = '.'
      const expected = ospath.join(process.cwd(), inputPath)
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should resolve relative path starting from user home directory if base value is ~', () => {
      const inputPath = 'path/to/file'
      const base = '~'
      const expected = ospath.join(os.homedir(), inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should resolve empty relative path', () => {
      const cwd = process.cwd()
      const tmp = os.tmpdir()
      const home = os.homedir()
      const inputPath = ''
      ;[
        [undefined, cwd],
        [tmp, tmp],
        ['~+', cwd],
        ['.', cwd],
        ['~', home],
      ].forEach(([base, expected]) => {
        const actual = expandPath(inputPath, { base })
        assert.equal(actual, expected)
      })
    })
  })

  describe('~', () => {
    it('should expand ~ to user home directory', () => {
      const inputPath = '~'
      const expected = os.homedir()
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })

    it('should expand leading ~ segment to user home directory', () => {
      const inputPath = '~/path/to/file'
      const expected = ospath.join(os.homedir(), 'path/to/file')
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })

    it('should not expand ~ at beginning of first segment', () => {
      const inputPath = '~path/to/file'
      const base = os.tmpdir()
      const expected = ospath.join(base, inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })
  })

  describe('~+', () => {
    it('should expand ~+ to cwd', () => {
      const inputPath = '~+'
      const expected = process.cwd()
      const actual = expandPath(inputPath, { base: os.tmpdir() })
      assert.equal(actual, expected)
    })

    it('should expand leading ~+ segment to cwd', () => {
      const inputPath = '~+/path/to/file'
      const expected = ospath.join(process.cwd(), 'path/to/file')
      const actual = expandPath(inputPath, { base: os.tmpdir() })
      assert.equal(actual, expected)
    })

    it('should use value of cwd parameter for cwd if specified', () => {
      const inputPath = '~+/path/to/file'
      const expected = ospath.join(os.homedir(), 'path/to/file')
      const actual = expandPath(inputPath, { cwd: os.homedir() })
      assert.equal(actual, expected)
    })

    it('should not expand ~+ at beginning of first segment', () => {
      const inputPath = '~+path/to/file'
      const base = os.tmpdir()
      const expected = ospath.join(base, inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })
  })

  describe('.', () => {
    it('should expand . to pwd if neither base or dot parameters are given', () => {
      const inputPath = '.'
      const expected = process.cwd()
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })

    it('should expand . to pwd if both base and dot parameters are undefined', () => {
      const inputPath = '.'
      const expected = process.cwd()
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })

    it('should expand . to base value if base parameter is given', () => {
      const inputPath = '.'
      const base = os.tmpdir()
      const expected = base
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should expand . to base value if base parameter is given and dot parameter is undefined', () => {
      const inputPath = '.'
      const base = os.tmpdir()
      const expected = base
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should expand . to dot value if dot parameter is given', () => {
      const inputPath = '.'
      const base = '~+'
      const dot = os.tmpdir()
      const expected = dot
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should expand . to user home directory if base value is ~ and dot parameter is not given', () => {
      const inputPath = '.'
      const base = '~'
      const expected = os.homedir()
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should expand . to user home directory if dot value is ~', () => {
      const inputPath = '.'
      const base = os.tmpdir()
      const dot = '~'
      const expected = os.homedir()
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should expand . to pwd if dot parameter is ~+', () => {
      const inputPath = '.'
      const base = os.tmpdir()
      const dot = '~+'
      const expected = process.cwd()
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should expand leading . segment to pwd if neither base or dot parameters are given', () => {
      const inputPath = './path/to/file'
      const expected = ospath.join(process.cwd(), 'path/to/file')
      const actual = expandPath(inputPath)
      assert.equal(actual, expected)
    })

    it('should expand leading . segment to base value if base parameter is given', () => {
      const inputPath = './path/to/file'
      const base = os.tmpdir()
      const expected = ospath.join(os.tmpdir(), 'path/to/file')
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should expand leading . segment to dot value if dot parameter is given', () => {
      const inputPath = './path/to/file'
      const base = os.tmpdir()
      const dot = ospath.join(base, 'subdir')
      const expected = ospath.join(dot, 'path/to/file')
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should expand leading . segment to user home directory if base value is ~ and dot parameter is not given', () => {
      const inputPath = './path/to/file'
      const base = '~'
      const expected = ospath.join(os.homedir(), inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })

    it('should expand leading . segment to user home directory if dot value is ~', () => {
      const inputPath = './path/to/file'
      const base = os.tmpdir()
      const dot = '~'
      const expected = ospath.join(os.homedir(), inputPath)
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should expand leading . segment to pwd if dot parameter is ~+', () => {
      const inputPath = './path/to/file'
      const base = os.tmpdir()
      const dot = '~+'
      const expected = ospath.join(process.cwd(), inputPath)
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should resolve path with leading ./.. segments relative to dot value if dot parameter is given', () => {
      const inputPath = './../path/to/file'
      const base = process.cwd()
      const dot = os.homedir()
      const expected = ospath.join(dot, '../path/to/file')
      const actual = expandPath(inputPath, { base, dot })
      assert.equal(actual, expected)
    })

    it('should not expand . at beginning of first segment', () => {
      const inputPath = '.path/to/file'
      const base = os.tmpdir()
      const expected = ospath.join(base, inputPath)
      const actual = expandPath(inputPath, { base })
      assert.equal(actual, expected)
    })
  })

  describe('positional base argument', () => {
    it('should allow base to be specified as second positional argument', () => {
      const inputPath = 'path/to/file'
      const base = os.tmpdir()
      const expected = ospath.join(base, inputPath)
      const actual = expandPath(inputPath, base)
      assert.equal(actual, expected)
    })

    it('should allow base to be specified as second positional argument and be empty', () => {
      const inputPath = 'path/to/file'
      const base = ''
      const expected = ospath.join(process.cwd(), inputPath)
      const actual = expandPath(inputPath, base)
      assert.equal(actual, expected)
    })
  })
})
